package com.lab.toik12.component.impl;

import com.lab.toik12.component.CsvOperationsComponent;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class CsvOperationsComponentImpl implements CsvOperationsComponent {

    private final List<List<Integer>> sudoku;

    public CsvOperationsComponentImpl() {
        sudoku = new ArrayList<>();
        readCsvFile();
    }


    @Override
    public void readCsvFile() {

        try {
            BufferedReader reader = new BufferedReader(new FileReader(PATH));

            String line;
            int i = 0;
            while ((line = reader.readLine()) != null) {
                sudoku.add(new ArrayList<>());
                for (int j = 0; j < 9; j++) {
                    int digit = Integer.parseInt((line.split(";")[j]));
                    sudoku.get(i).add(digit);
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<List<Integer>> getSudoku() {
        return sudoku;
    }

}
