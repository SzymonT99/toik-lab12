package com.lab.toik12.component;

import java.util.List;

public interface CsvOperationsComponent {

    String PATH = "src/main/resources/sudoku.csv";

    void readCsvFile();

    List<List<Integer>> getSudoku();

}
