package com.lab.toik12.rest;

import com.lab.toik12.dto.WrongSudokuDto;
import com.lab.toik12.service.SudokuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SudokuController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SudokuController.class);

    private final SudokuService sudokuService;

    @Autowired
    public SudokuController(SudokuService sudokuService) {
        this.sudokuService = sudokuService;
    }

    @GetMapping("/api/sudoku/verify")
    public ResponseEntity<?> verify() {

        WrongSudokuDto wrongSudokuDto = sudokuService.verifySudoku();

        LOGGER.info("--- Dokonano weryfikacji sudoku");

        return wrongSudokuDto != null
                ? new ResponseEntity<>(wrongSudokuDto, HttpStatus.BAD_REQUEST)
                : new ResponseEntity<>(HttpStatus.OK);

    }
}
