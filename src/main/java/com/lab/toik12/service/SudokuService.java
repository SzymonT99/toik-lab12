package com.lab.toik12.service;

import com.lab.toik12.dto.WrongSudokuDto;

public interface SudokuService {

    WrongSudokuDto verifySudoku();
}
