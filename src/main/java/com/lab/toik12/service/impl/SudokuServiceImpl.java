package com.lab.toik12.service.impl;
import com.lab.toik12.component.CsvOperationsComponent;
import com.lab.toik12.dto.WrongSudokuDto;
import com.lab.toik12.service.SudokuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class SudokuServiceImpl implements SudokuService {

    private final CsvOperationsComponent csvOperationsComponent;

    @Autowired
    public SudokuServiceImpl(CsvOperationsComponent csvOperationsComponent) {
        this.csvOperationsComponent = csvOperationsComponent;
    }

    @Override
    public WrongSudokuDto verifySudoku() {

        List<List<Integer>> sudoku =  csvOperationsComponent.getSudoku();

        List<Integer> lineIds = new ArrayList<>();
        List<Integer> columnIds = new ArrayList<>();
        List<Integer> areaIds = new ArrayList<>();

        //sprawdzanie wierszy

        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                if (!(sudoku.get(row).contains(col + 1) && Collections.frequency(sudoku.get(row), col + 1) == 1)) {
                    lineIds.add(row + 1);
                    break;
                }
            }
        }

        //sprawdzanie kolumn

        for (int col = 0; col < 9; col++) {
            for (int row = 0; row < 9; row++) {
                int counter = 0;
                for (int index = 0; index < 9; index++) {
                    if (sudoku.get(index).get(col) == row + 1) {
                        counter++;
                    }
                }
                if (counter > 1 || counter == 0) {
                    columnIds.add(col + 1);
                    break;
                }
            }
        }

        //sprawdzanie kwadratów

        List<List<Integer>> areaList = new ArrayList<>();

        for (int i = 0; i < 9; i++) {
            areaList.add(new ArrayList<>());
        }

        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                if (row < 3 && col < 3) {
                    areaList.get(0).add(sudoku.get(row).get(col));
                }
                else if (row < 3 && (col > 2 && col < 6)) {
                    areaList.get(1).add(sudoku.get(row).get(col));
                }
                else if (row < 3 && col > 5) {
                    areaList.get(2).add(sudoku.get(row).get(col));
                }
                else if ((row > 2 && row < 6) && col < 3) {
                    areaList.get(3).add(sudoku.get(row).get(col));
                }
                else if ((row > 2  && row < 6) && (col > 2 && col < 6)) {
                    areaList.get(4).add(sudoku.get(row).get(col));
                }
                else if ((row > 2  && row < 6) && col > 5) {
                    areaList.get(5).add(sudoku.get(row).get(col));
                }
                else if (row > 5 && col < 3) {
                    areaList.get(6).add(sudoku.get(row).get(col));
                }
                else if (row > 5 && (col > 2 && col < 6)) {
                    areaList.get(7).add(sudoku.get(row).get(col));
                }
                else if (row > 5 && col > 5) {
                    areaList.get(8).add(sudoku.get(row).get(col));
                }
            }
        }

        for (int id = 0; id < 9; id++) {
            for (int el = 0; el < 9; el++) {
                if (!(areaList.get(id).contains(el + 1) && Collections.frequency(areaList.get(id), el + 1) == 1)) {
                    areaIds.add(id + 1);
                    break;
                }
            }
        }

        boolean isGood = lineIds.isEmpty() || columnIds.isEmpty() || areaIds.isEmpty();

        if(isGood) {
            return null;
        }
        else {
            return new WrongSudokuDto(lineIds, columnIds, areaIds);
        }

    }
}
